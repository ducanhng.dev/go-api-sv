module go-api-sv

go 1.16

require (
	github.com/DATA-DOG/go-sqlmock v1.5.0
	// github.com/aws/aws-sdk-go v1.40.20
	github.com/gin-gonic/gin v1.7.3
	github.com/go-playground/validator/v10 v10.4.1
	github.com/golang-jwt/jwt v3.2.2+incompatible
	github.com/sirupsen/logrus v1.8.1
	// github.com/lib/pq v1.10.2
	github.com/stretchr/testify v1.5.1
	golang.org/x/crypto v0.0.0-20210817164053-32db794688a5
	gorm.io/driver/mysql v1.1.2
	gorm.io/driver/postgres v1.1.0
	gorm.io/gorm v1.21.12
	gorm.io/plugin/dbresolver v1.1.0
)
