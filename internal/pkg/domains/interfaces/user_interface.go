package interfaces

import "go-api-sv/internal/pkg/domains/models/entities"

// UserRepository interface
type UserRepository interface {
	Find() ([]entities.User, error)
	Create(user entities.User) (entities.User, error)
	TakeByID(id uint) (entities.User, error)
	TakeByUsername() (entities.User, error)
	TakeByEmail() (entities.User, error)
}

// UserUsecase interface
type UserUsecase interface {
	Find() ([]entities.User, error)
	Create(user entities.User) (entities.User, error)
	TakeByID(id uint) (entities.User, error)
	TakeByUsername() (entities.User, error)
	TakeByEmail() (entities.User, error)
}
